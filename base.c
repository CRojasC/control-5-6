#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>

float desvStd(estudiante curso[]){
 	float promg = 0, base, diferencia, diferencia2, desviacion = 0;
 	int x = 0, cont = 0, suma;
//Este primer while es para saber cual es la cantidad de aluimnos que hay en el curso
 	while(curso[x].prom != 0.0){
 		promg += curso[x].prom;
 		x++;
 		cont++;
 	}
 	if(cont == 0){
 		printf("no se han ingresado notas\n");
 		return 0;
 	}
 	promg = promg/cont;//aqui se ssaca el promedio general del curso
 	x = 0;
//aqui se saca la desviacion estandar del curso
 	while (x<cont){
 		diferencia = curso[x].prom-promg;
 		diferencia2 = diferencia*diferencia;
 		suma += diferencia2;
 		x++;
 	}
 	base = suma/cont;
 	desviacion= sqrt(base);
 	return desviacion;
}
float menor(estudiante curso[]){
 	float menor = 7.0;
 	int x = 0,cont=0;
//nuevamente se cuanta la cantidad ed alumnos
 	while(strcmp(curso[x].nombre,"\0") != 0){
 		x++;
 		cont++;
	x=0;
 	int alumnos =cont;
//aqui se recorre cada alumno, para asi poder sacar el promedio mas bajo del curso
 	for(x= 0;x<alumnos;x++){
 		if(menor > curso[x].prom && curso[x].prom != 0 ){
 			menor = curso[x].prom;
 		}
 	}
 	return menor;
}
}
float mayor(estudiante curso[]){
 	float mayor = 1.0;
 	int x = 0;
//nuevamente se cuanta la cantidad de  alumnos
 	while(curso[x].prom != 0){
//aqui se saca el promedio mayor del curso
 		if(mayor<curso[x].prom){
 			mayor = curso[x].prom;
 		}
 		x++;
 	}
 	return mayor;
}
void registroCurso(estudiante curso[]){
	int x,prom[24],prom_p; //aqui se define las variables a utilizar, designando un rango para el promedio
	for(x=0;x<24;x++){ // en esta parte se define el rango para poder recorrer toda la lista
		curso[x].prom=0;
		printf("para el estudiante %s %s %s:\n",curso[x].nombre,curso[x].apellidoP,curso[x].apellidoM);//Aqui en esta parte nombra al estudiante para asi despues determinarle su nota y promedio correspondiente
//aqui se pide todo para asi poder sacarle el porcentaje que valdra cada nota para poder lelgar al promedio final
		printf("ingrese la nota1\n");
		scanf("%f",&curso[x].asig_1.proy1);
		prom_p=prom_p+(curso[x].asig_1.proy1 * 0.20);
 		printf("ingrese la nota 2\n");
 		scanf("%f",&curso[x].asig_1.proy2);
 		prom_p=prom_p+(curso[x].asig_1.proy2 * 0.20);
 		printf("ingrese la nota 3\n");
 		scanf("%f",&curso[x].asig_1.proy3);
 		prom_p=prom_p+(curso[x].asig_1.proy3 * 0.30);
 		printf("ingrese la nota del control 1\n");
 		scanf ("%f",&curso[x].asig_1.cont1);
 		prom_p=prom_p+(curso[x].asig_1.cont1 * 0.05);
 		printf("ingrese la nota del control 2\n");
 		scanf ("%f",&curso[x].asig_1.cont2);
 		prom_p=prom_p+(curso[x].asig_1.cont2 * 0.05);
 		printf("ingrese la nota del control 3\n");
 		scanf ("%f",&curso[x].asig_1.cont2);
 		prom_p=prom_p+(curso[x].asig_1.cont3 * 0.05);
 		printf("ingrese la nota del control 4\n");
 		scanf ("%f",&curso[x].asig_1.cont2);
 		prom_p=prom_p+(curso[x].asig_1.cont4 * 0.05);
 		printf("ingrese la nota del control 5\n");
 		scanf ("%f",&curso[x].asig_1.cont2);
 		prom_p=prom_p+(curso[x].asig_1.cont5 * 0.05);
 		printf("ingrese la nota del control 6\n");
 		scanf ("%f",&curso[x].asig_1.cont2);
 		prom_p=prom_p+(curso[x].asig_1.cont6 * 0.05);
// aqui en donde finalmente depues de calcular los porcentajes de cada nota y modificarlo al promedio aprcial, sumamos todos los porcentajes pra asi obetener el promedio final
 		prom[x]=(curso[x].asig_1.proy1 * 0.20)+(curso[x].asig_1.proy2 * 0.20)+(curso[x].asig_1.proy3 * 0.30)+(curso[x].asig_1.cont1 * 0.05)+(curso[x].asig_1.cont2 * 0.05)+(curso[x].asig_1.cont3 * 0.05)+(curso[x].asig_1.cont4 * 0.05)+(curso[x].asig_1.cont5 * 0.05)+(curso[x].asig_1.cont6 * 0.05);
 	}		 
}
void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *npaso;//aqui se crea un puntero para los que no aprueban
 	FILE *paso;//aqui se crea un puntero para los que aprueban
 	char lst_nopaso[2048]="",lst_paso[2048]="";//aqui se generan las lsitas para guardar a los estudiantes
 	int x=0,True=1;
//generadores de los documentes txt
 	paso=fopen("pasaron.txt","w");
 	npaso=fopen("nopasaron.txt","w");
 	while(curso[x].prom != 0.0){
 		if (curso[x].prom< 4.0 && curso[x].prom != 0){
//En esta parte se ingrasan a la lista de los que no pasaron los nombres, apellidos y se genera un docmunto txt donde se guarda todo esto
 			strcat(lst_nopaso,curso[x].nombre);
 			strcat(lst_nopaso, " ");
 			strcat(lst_nopaso,curso[x].apellidoP);
 			strcat(lst_nopaso, " ");
 			strcat(lst_nopaso, curso[x].apellidoM);
 			strcat(lst_nopaso, " ");
 			fprintf(npaso,"%s %2f\n",lst_nopaso,curso[x].prom);
 			strcpy(lst_paso,"");
 		}
 		else{
//ahora en esta parte apsa exactamente lo mismo pero con los que pasaron
 			strcat(lst_paso,curso[x].nombre);
 			strcat(lst_paso, " ");
 			strcat(lst_paso,curso[x].apellidoP);
 			strcat(lst_paso, " ");
 			strcat(lst_paso, curso[x].apellidoM);
 			strcat(lst_paso, " ");
 			fprintf(paso, "%s %2f\n",lst_paso,curso[x].prom);
 			strcpy(lst_nopaso,"");
			}
 		}
 	}

void metricasEstudiantes(estudiante curso[]){
//Aqui se da a conocer la desviacion estandar, el proemdio mayor y el promedio menor
	float desviacion=desvStd(curso),prom_mayor=mayor(curso),prom_menor=menor(curso);
	printf("\n la desviacion estandar es: %3f\n el mejor promedio fue:%2f\nel menor promedio fue: %2f\n",desviacion,prom_mayor,prom_menor);
}
void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}
